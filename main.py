from machine import Pin, SoftI2C
from ssd1306 import SSD1306_I2C
import framebuf
import utime
import freesans20
import writer

trigger = Pin(3, Pin.OUT)
echo = Pin(2, Pin.IN)
 
WIDTH  = 128
HEIGHT = 64
 
i2c = SoftI2C( scl=Pin(1), sda=Pin(0))       
oled = SSD1306_I2C(WIDTH, HEIGHT, i2c)

def ultrasonnic():
    timepassed=0
    trigger.low()
    utime.sleep_us(2)
    trigger.high()
    utime.sleep_us(10)
    trigger.low()
    while echo.value() == 0:
        signaloff = utime.ticks_us()
    while echo.value() == 1:
        signalon = utime.ticks_us()
    timepassed = signalon - signaloff
    distance_cm = (timepassed * 0.0343) / 2
    distance_cm = round(distance_cm)
    return distance_cm 
 
while True:
    oled.fill(0)  
    distance_cm = ultrasonnic() 
    oled.text("The bored Eng.",0,0)
    font_writer = writer.Writer(oled, freesans20)
    font_writer.set_textpos(32, 32)
    font_writer.printstring(str(distance_cm)+" cm")
    print(str(distance_cm)+" cm")
    oled.rect(5, 20, 120, 43, 1)
    oled.show()
    utime.sleep(0.2)

 

 